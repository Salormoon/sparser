Platform
========

.. automodule:: sp.platform

.. autoclass:: sp.platform.Platform

    .. autoattribute:: users
    .. autoproperty:: view

    Упрвление хранилищем
    --------------------

    .. automethod:: get_user
    .. automethod:: get_intents

    Сокращения дле методов представления
    ------------------------------------

    .. automethod:: lessons
    .. automethod:: today_lessons
    .. automethod:: current_day
    .. automethod:: relative_day
    .. automethod:: search
    .. automethod:: counter
    .. automethod:: update
    .. automethod:: status
