Counters
========

.. automodule:: sp.counter

.. autoclass:: CounterTarget


Вспомогательные функции
-----------------------

Используеются для поддержания работы основных функций счётчиков.

.. note:: Устаревшие функции?
    Обратите внимение что возможно в будущих версиях данные функции
    полностью исчезнут.

.. autofunction:: reverse_counter


Класс счётчика
--------------

.. autoclass:: CurrentCounter

    .. automethod:: cl
    .. automethod:: days
    .. automethod:: index
