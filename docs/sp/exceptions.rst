exceptions
==========

.. automodule:: sp.exceptions

View
----

Исключения, которые могут возникнуть при работе с классом представления.

.. hint:: О классе представления

    В далёкой sp v7 классом представления станет особый класс
    способный представить расписание в одном из удобных форматов.
    Среди них у нас будет :py:class:`sp.messages.SPMessages`.

.. autoexception:: ViewCompatibleError
.. autoexception:: ViewSelectedError
