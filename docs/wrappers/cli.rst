SPCli
=====

.. image:: _images/spm_console.png

CLI (command Line Interfate) - Интерфейс командной строки.
Самая простая оболочка для прямого доступа к SPMessages.
Не требует для работы дополнительных зависимостей.

Эта оболочка в силу своей простоты первая реализует все возможности
послежней версии генератора сообщений.

Что-ж, давайте перейдём от слова к делу.


Использование
-------------

Чтож, вы можете в любой момент получить справку с описанием
всех ключей.

.. code-block:: shell

    # Чтобы получить справку по использованию консоли
    python spcli.py --help

Первый делом, как и в любой другой обёртке, вам нужно укзаать класс
по умолчанию для более удобной работы с генератором сообщений.

.. attention:: Класс по умолчанию.

    Конесно, вы можете и не указывать класс, но тогда некоторые
    функции будут недоступны.
    Подробнее о том, какие преимущества даёт указанный класс вы можеет
    узнать в пользовательской документации, в разделе выбора класса
    по умолчанию.

Для того чтобы указать классс по умолчанию используется ключ `--set-class` с
последующим укзанием класса.

.. code-block:: shell

    # Устанавливаем класс по умолчанию
    python spcli.py --set-class "8в"
